import React from 'react'
import { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import "./MovieCard.css";

const MovieCard = (props) => {
    return (
        <div className='card'>
            <div className='poster'>
                <img
                    className='MovieCard-poster'
                    src={props.poster}
                    alt={props.title}
                />
            </div>

            <div className='info'>
                <p className='title'>{props.title}</p>
                <p className="date">{props.date}</p>
                <p className="rating">{props.rating}</p>
            </div>
        </div>

    );

}

MovieCard.defaultProps = {
    poster:
        "https://d994l96tlvogv.cloudfront.net/uploads/film/poster/poster-image-coming-soon-placeholder-no-logo-500-x-740_28343.png",
    title: "Title",
    date: "Date",
    rating: "Insert Rating"
};

MovieCard.propTypes = {
    title: PropTypes.string,
    poster: PropTypes.string,
    date: PropTypes.string,
    rating: PropTypes.string,
}

export default MovieCard;