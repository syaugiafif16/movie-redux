import { Box, Card, CardContent, Typography } from '@mui/material'
import { Button, CardActions } from '@mui/material'


export const PanelCard = () => {
    return (
        <Box width='280px'>
            <Card>
                <CardContent>
                    <Typography gutterBottom variant='body2' component='text.secondary'>
                        <h5>Sort Movies by..</h5>
                    </Typography>
                </CardContent>

                <CardActions>

                </CardActions>
            </Card>


        </Box>
    )
}
export default PanelCard;