
import './App.css';

import Header from './components/Header';
import PanelCard from './components/PanelCard';
import * as React from 'react';
import { Button } from 'react-bootstrap';
import { TextField } from '@mui/material';
import MovieList from './MovieList';
import { useGetPokemonsQuery } from './api/pokemon';

function App() {

  const { isLoading } = useGetPokemonsQuery();

  return (
    <>

      <Header>
        <div className='App'>Movie App</div>
      </Header>

      <div className='panel'>

        <h2 className='h2'>Upcoming Movies</h2>
        <PanelCard />
        <TextField id="tf" label="MOVIE NAME" variant="outlined" />
        <Button type="submit" id='btn' color="primary" sx={{ borderRadius: 1 }}>Search</Button>

      </div>
      {isLoading ? 
        (
          <h1>Lagi Loading Bro</h1>
        )
      : (
        <MovieList />
      )}
      
    </>
  );
}

export default App;
