import MovieCard from './components/MovieCard';
import { useSelector, useDispatch } from 'react-redux'

const MovieList = () => {
    
    const pokemons = useSelector((state) => state.pokemons.lists)
    
    return (
        <div className='container'>
            {pokemons.map((pokemon, index) => (
                <div className='movie-wrapper' key={index}>
                    <MovieCard
                        title={pokemon.name}
                        poster="https://m.media-amazon.com/images/I/815IBWT+WeL.jpg"
                        date="Mar 1, 2023"
                        rating="9.5" />
                </div>

            ))}
        </div>
    )
}

export default MovieList