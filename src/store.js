import { configureStore } from '@reduxjs/toolkit'
import pokemonsReducer from './features/pokemonsSlice'

import { pokemonApi } from './api/pokemon'

export const store = configureStore({
  reducer: {
    pokemons: pokemonsReducer,
    [pokemonApi.reducerPath]: pokemonApi.reducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(pokemonApi.middleware),
})