import { createSlice } from '@reduxjs/toolkit'
import { pokemonApi } from '../api/pokemon'

const initialState = {
  lists: [],
}

export const pokemonsSlice = createSlice({
  name: 'pokemons',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addMatcher(
      pokemonApi.endpoints.getPokemons.matchFulfilled,
      (state, { payload }) => {
        state.lists = payload.results
      }
    )
  },
})


export default pokemonsSlice.reducer